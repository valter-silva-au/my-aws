#!/bin/bash

# Create a simple script, using your language of choice, that creates an AWS EC2 instance and outputs its launch time. The script should:
#
# * Utilize the AWS API
# * Accept the arguments: AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_REGION, INSTANCE_TYPE
# * Wait for an instance to come online, and then return the instance launch time to STDOUT

# set -x

readonly PROGNAME=$(basename $0)
readonly PROGDIR=$(readlink -m $(dirname $0))
readonly ARGS="$@"

main(){
  echo $ARGS
  cmdline $ARGS
  if [ -z ${SECURE} ]; then
    create_ec2
  else
    create_ec2_secured
  fi
}

create_ec2_secured(){
  local start=$(date)
  local vpcId=$(aws ec2 create-vpc --cidr-block 10.0.0.0/28 --query 'Vpc.VpcId' --output text)

  # Setting up your VPC
  aws ec2 modify-vpc-attribute --vpc-id ${vpcId} --enable-dns-support "{\"Value\":true}" > /dev/null
  aws ec2 modify-vpc-attribute --vpc-id ${vpcId} --enable-dns-hostnames "{\"Value\":true}" > /dev/null

  # Adding an Internet Gateway
  local internetGatewayId=$(aws ec2 create-internet-gateway --query 'InternetGateway.InternetGatewayId' --output text)
  aws ec2 attach-internet-gateway --internet-gateway-id ${internetGatewayId} --vpc-id ${vpcId} > /dev/null

  # Creating a Subnet
  local subnetId=$(aws ec2 create-subnet --vpc-id ${vpcId} --cidr-block 10.0.0.0/28 --query 'Subnet.SubnetId' --output text)

  # Configuring the Route Table
  local routeTableId=$(aws ec2 create-route-table --vpc-id ${vpcId} --query 'RouteTable.RouteTableId' --output text)
  aws ec2 associate-route-table --route-table-id ${routeTableId} --subnet-id ${subnetId} > /dev/null
  aws ec2 create-route --route-table-id ${routeTableId} --destination-cidr-block 0.0.0.0/0 --gateway-id ${internetGatewayId} > /dev/null

  # Adding a Security Group
  local securityGroupId=$(aws ec2 create-security-group --group-name my-security-group --description "my-security-group" --vpc-id $vpcId --query 'GroupId' --output text)
  aws ec2 authorize-security-group-ingress --group-id $securityGroupId --protocol tcp --port 22 --cidr 0.0.0.0/0 > /dev/null

  # If a key pair was not provided, a new one will be created and used
  if [ ! -f ${PROGDIR}/../my-key.pem ]; then
    aws ec2 create-key-pair --key-name my-key --query 'KeyMaterial' --output text > ${PROGDIR}/../my-key.pem
    chmod 400 ${PROGDIR}/../my-key.pem
  fi

  # Launching your Instance
  local instanceId=$(aws ec2 run-instances \
  --image-id ami-1c45e273 \
  --count 1 \
  --instance-type ${INSTANCE_TYPE} \
  --key-name my-key \
  --security-group-ids $securityGroupId \
  --subnet-id $subnetId \
  --associate-public-ip-address --query 'Instances[0].InstanceId' --output text)

  echo "Launch time: $(aws ec2 describe-instances --instance-ids "${instanceId}" --query 'Reservations[0].Instances[0].LaunchTime')"
}

create_ec2(){
  # aws ec2 run-instances --image-id ami-1a2b3c4d --count 1 --instance-type c3.large --key-name MyKeyPair --security-groups MySecurityGroup
  local instanceId=$(aws ec2 run-instances \
  --image-id ami-1c45e273 \
  --count 1 \
  --instance-type ${INSTANCE_TYPE} \
  --key-name my-key \
  --security-groups default \
  --query 'Instances[0].InstanceId' --output text)

  echo "Launch time: $(aws ec2 describe-instances --instance-ids "${instanceId}" --query 'Reservations[0].Instances[0].LaunchTime')"
}


usage(){
cat <<- EOF
  usage: $PROGNAME options

  Script creates an AWS EC2 instance and outputs its launch time.

  OPTIONS:
     -a --access-key          a valid AWS access key.
     -e --secure              creates an ec2 instance in a VPC.
     -s --secret-access-key   secret access key from the provided access key.
     -r --region              AWS datacenter region.
     -i --instance-type       instance type of the ec2 instance to be created.
     -v --verbose             Verbose. You can specify more then one -v to have more verbose
     -x --debug               debug
     -h --help                show this help

  Examples:
     Run:
     $PROGNAME --access-key --access-key XYZ --secret-access-key ABC --region [eu-central-1|us-east-1] -instance-type [t2.micro|t2.small]

EOF
}

cmdline() {
  local arg=
  for arg
  do
      local delim=""
      case "$arg" in
          #translate --gnu-long-options to -g (short options)
          --access-key)     args="${args}-a ";;
          --secure)             args="${args}-e ";;
          --help)               args="${args}-h ";;
          --instance-type)      args="${args}-i ";;
          --region)             args="${args}-r ";;
          --secret-access-key)  args="${args}-s ";;
          --debug)              args="${args}-x ";;
          #pass through anything else
          *) [[ "${arg:0:1}" == "-" ]] || delim="\""
              args="${args}${delim}${arg}${delim} ";;
      esac
  done

  #Reset the positional parameters to the short options
  eval set -- $args

  while getopts "ehvx:a:s:r:i:c:" OPTION
  do
       case $OPTION in
       a)
           export AWS_ACCESS_KEY_ID=$OPTARG
           ;;
       e)
           readonly SECURE=1
           ;;
       h)
           usage
           exit 0
           ;;
       i)
           readonly INSTANCE_TYPE=$OPTARG
           ;;
       r)
           export AWS_DEFAULT_REGION=$OPTARG
           export AWS_DEFAULT_OUTPUT=json
           ;;
       s)
           export AWS_SECRET_ACCESS_KEY=$OPTARG
           ;;
       x)
           readonly DEBUG='-x'
           set -x
           ;;
      esac
  done

  if [[ -z ${AWS_ACCESS_KEY_ID} || -z ${AWS_SECRET_ACCESS_KEY} || -z ${AWS_DEFAULT_REGION} || -z ${INSTANCE_TYPE} ]]; then

      [[ -z ${AWS_ACCESS_KEY_ID} ]] \
        && exit "You must provide --access_key_id"

      [[ -z ${AWS_SECRET_ACCESS_KEY} ]] \
        && exit "You must provide --secret_access_key"

      [[ -z ${AWS_DEFAULT_REGION} ]] \
        && exit "You must provide --region"

      [[ -z ${INSTANCE_TYPE} ]] \
        && exit "You must provide --instance-type"
  fi
  return 0
}

main
