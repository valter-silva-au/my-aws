My AWS
================================

In order to increase our productivity while using AWS, our engineers created these scripts, separated by the service name.

- - -
![AWS](https://cdn2.hubspot.net/hub/325374/file-457202904-png/AWS.png)

## Requirements

* AWS Command Line Interface

You can follow [these instructions](http://docs.aws.amazon.com/cli/latest/userguide/installing.html).

## Security

In order to create `EC2` instance, put your `key pair` inside of this folder and rename it as `my-key.pem`.
This file is ignored, and SHOULD NOT be tracked by Git.

## Run

You can create an EC2 instance in 2 ways:

* Unsecured (no VPC)

```
./ec2/create-ec2.sh --access-key ABC --secret-access-key XYZ --region eu-central-1 --instance-type t2.micro
```

*  Secured (with a VPC)

```
./ec2/create-ec2.sh --access-key ABC --secret-access-key XYZ --region eu-central-1 --instance-type t2.micro --secured
```

And both commands, should have the same output:

    Launch time: "2017-XX-XXTXX:XX:XX.000Z"

## Author

* **Valter Henrique** - [GitHub](https://github.com/valterhenrique)
